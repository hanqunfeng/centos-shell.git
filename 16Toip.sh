#!/bin/bash
#################################################
#功能：将十六进制IP地址转换为十进制形式
#filename：16Toip.sh
#执行方法：./16Toip.sh 0xffffff00
#输出结果：255.255.255.0
#################################################

# 检查是否提供了正确数量的参数
if [ "$#" -ne 1 ]; then
    echo "用法: 16Toip 0xffffff00"
    exit 1
fi

netmask=$1
# Remove the prefix '0x' and convert the hexadecimal to decimal ip address
ip_format=$(printf "%d.%d.%d.%d" $(echo ${netmask#0x} | sed 's/../0x& /g' | xargs printf "%d "))

echo $ip_format

