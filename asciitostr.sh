#!/bin/bash
#################################################
#filename：asciitostr.sh
#功能： ascii码转字符串
#执行方法：./asciitostr.sh \u4f60\u597d\u68\u65\u6c\u6c\u6f
#输出结果：你好hello
#################################################

# 检查是否提供了正确数量的参数
if [ "$#" -ne 1 ]; then
    echo "用法: $0 \u4f60\u597d\u68\u65\u6c\u6c\u6f"
    exit 1
fi

# 检查是否包含\u，如果不包含，添加双引号
if [[ "$1" != *\\u* ]]; then
  input=$(echo -e $1 | sed 's/u/\\u/g')
else
  input=$1
fi
# 将Unicode转义序列解析为中文字符
result=$(echo -e $input | perl -pe 's/\\u([0-9a-fA-F]{4})/chr(hex($1))/eg')

# 输出结果
echo "$result"

