#!/bin/bash
#################################################
# filename: check_domain.sh
# 功能: 检查域名到期时间
# 执行方法: ./check_domain.sh baidu.com google.com
# 输出结果：
# baidu.com，过期日期：2026-10-11
# 距离到期还有958天
# google.com，过期日期：2028-09-14
# 距离到期还有1662天
#################################################

# 验证域名参数是否为空
if [[ ${#@} < 1 ]];then
    echo "请输入域名，多个域名空格分隔，如：./check_domain.sh baidu.com google.com"
fi

# 检测域名到期时间
for domain in "$@"; do
    #echo $domain
    # 取出域名过期时间
    expire_date=$(whois $domain | grep -E 'Expiration Time' | awk '{print $3}')
    if [ -z "$expire_date" ]; then
        expire_date=$(whois $domain | grep 'Expiry Date' | awk '{print $4}' | cut -d 'T' -f 1)
    fi
    echo "${domain}，过期日期：${expire_date}"

    # 转换成时间戳
    expire_date_timestamp=$(date -d $expire_date +%s)

    # 以时间戳的形式显示当前时间
    now=$(date '+%s')

    # 域名到期剩余天数
    time_left=$(( ($expire_date_timestamp-$now)/86400 ))
    echo "距离到期还有${time_left}天"
done

