#!/bin/bash
#################################################
# filename: ip-info.sh
# 功能: 查看IP信息
# 使用方法: ./ip-info.sh 10.10.2.24/24
# 输出结果：
#  网络地址: 10.10.2.0
#  网关地址: 10.10.2.1
#  广播地址: 10.10.2.255
#  主机地址范围: 10.10.2.2 到 10.10.2.254

# 检查是否提供了正确数量的参数
if [ "$#" -ne 1 ]; then
    echo "用法: $0 <IP地址/子网掩码CIDR>"
    exit 1
fi

# 获取命令行参数
ip_cidr="$1"

# 分割IP地址和子网掩码
IFS='/' read -ra ip_parts <<< "$ip_cidr"
ip="${ip_parts[0]}"
subnet_cidr="${ip_parts[1]}"

# 计算网络地址
network_address=$(ipcalc -n "$ip_cidr" | awk -F'=' '{print $2}')

# 计算广播地址
broadcast_address=$(ipcalc -b "$ip_cidr" | awk -F'=' '{print $2}')

# 计算网关地址
gateway_address=$(ipcalc -n "$ip_cidr" | awk -F'=' '{print $2}' | awk -F'.' '{print $1"."$2"."$3"."($4+1)}')

# 输出结果
echo "网络地址: $network_address"
echo "网关地址: $gateway_address"
echo "广播地址: $broadcast_address"

# 计算主机地址范围
first_host=$(ipcalc -n "$ip_cidr" | awk -F'=' '{print $2}' | awk -F'.' '{print $1"."$2"."$3"."($4+1)}')
last_host=$(ipcalc -b "$ip_cidr" | awk -F'=' '{print $2}' | awk -F'.' '{print $1"."$2"."$3"."($4-1)}')
echo "主机地址范围: $first_host 到 $last_host"

