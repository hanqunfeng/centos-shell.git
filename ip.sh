#!/bin/bash
#################################################
# filename: ip.sh
# 执行方法: ./ip.sh
# 功能: 查看本机网络信息
#################################################

# 默认获取外网ip超时3秒
maxtime=3

net_ip=$(curl --max-time $maxtime --silent https://ipv4.icanhazip.com)
MAC=$(ifconfig | grep eth0: -A 7 | grep ether | awk -F " " '{print $2}')
local_ip=$(ifconfig | grep eth0: -A 7 | grep broadcast | awk -F " " '{print $2}')
DNS=$(nslookup localhost | grep Server | awk '{print $2}')

echo "MAC: ${MAC}"
echo "IP: ${local_ip} ${net_ip}"
echo "DNS: ${DNS}"

netmask=$(ifconfig | grep eth0: -A 7 | grep broadcast | awk -F " " '{print $4}')
one_count=$(ipTo2.sh $netmask  | awk -F "1" '{print NF-1}')
echo "NETMASK: $netmask,/${one_count}"

echo -e "\nIP详细信息:"
ip-info.sh ${local_ip}/${one_count}

