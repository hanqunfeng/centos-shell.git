#!/bin/bash
#################################################
# filename: killport.sh
# 功能: 关闭占用端口的进程
# 执行方法: ./killport.sh 8080
#################################################

if [ -z "$1" ]; then
    echo "请加上端口作为参数，如：./killport.sh 8080"
else
    echo "kill begin........"
    pid=$(netstat -tunpl | awk -v port="$1" '$4 ~ ":"port"$" && NR>2 {print $7}' | cut -d'/' -f1 | sort | uniq)
    echo $pid
    if [ -z "$pid" ]; then
        echo "No process running."
    else
        echo "The process (${pid}) is running..."
        kill ${pid}
        echo "kill process (${pid}) OK"
    fi
fi

