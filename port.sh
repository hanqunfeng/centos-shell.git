#!/bin/bash
#################################################
# filename: port.sh
# 功能: 查询端口被哪个进程占用
# 执行方法: ./port.sh 8080
#################################################

port=$1

if [ -z "$port" ]; then
    echo "请加上端口作为参数，如：./port.sh 8080"
else
   # netstat -tunpl | grep ":${port}"
   netstat -tunpl | awk -v port="$port" '$4 ~ ":"port"$"'
fi

