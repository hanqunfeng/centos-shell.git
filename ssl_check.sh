#!/bin/bash
#################################################
# filename: ssl_check.sh
# 功能: 检查证书到期时间
# 执行方法: ./ssl_check.sh www.baidu.com www.google.com
# 输出结果：
# 证书：www.baidu.com，过期时间：2024-08-06 01:51:05
# 距离到期还有161天
# 证书：www.google.com，过期时间：2024-04-29 08:19:49
# 距离到期还有62天
#################################################

# 验证证书参数是否为空
if [[ ${#@} < 1 ]];then
    echo "请输入证书，多个证书空格分隔，如：./ssl_check.sh www.baidu.com www.google.com"
fi

# 获取证书的有效时间
for domain in "$@"; do
    # 这里在openssl x509 -noout -dates后面加上了2>/dev/null，否则会打印告警信息：Warning: Reading certificate from stdin since no -in or -new option is given
    # 您可以忽略此警告。它只是表明您没有使用 -in 或 -new 选项指定输入文件，因此 OpenSSL 将从标准输入 (stdin) 读取证书。
    time=$(echo | openssl s_client -connect $domain:443 2>/dev/null | openssl x509 -noout -dates 2>/dev/null | awk -F'=' 'NR==2{print $2}')
    # echo $time

    # 取出域名过期时间
    expire_date=$(env LC_ALL=en_US.en date -d "$time" '+%Y-%m-%d %H:%M:%S')
    echo "证书：$domain，过期时间：$expire_date"

    expire_date_timestamp=$(date -d "$expire_date" +%s)
    # 以时间戳的形式显示当前时间
    now=$(date '+%s')
    # 证书到期剩余天数
    time_left=$(( ($expire_date_timestamp-$now)/86400 ))
    echo "距离到期还有${time_left}天"
done

