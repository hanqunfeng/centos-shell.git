#!/bin/bash
#################################################
#filename：strtoascii.sh
#功能：字符串转ascii码
#执行方法：./strtoascii.sh 你好hello
#输出结果：\u4f60\u597d\u68\u65\u6c\u6c\u6f
#################################################

# 检查是否提供了正确数量的参数
if [ "$#" -ne 1 ]; then
    echo "用法: $0 你好hello"
    exit 1
fi

str=$1
newstr=""

for ((i=0;$i<${#str};i=$i+1));
do
    text="${str:$i:1}"
    newtext=$(printf "%x" "'$text")
    newstr=$newstr"\\u"$newtext
done
echo $newstr

